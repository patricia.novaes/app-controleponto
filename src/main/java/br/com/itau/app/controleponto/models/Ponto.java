package br.com.itau.app.controleponto.models;

import br.com.itau.app.controleponto.enums.TipoPontoEnum;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Ponto {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPonto;

    private LocalDateTime horarioBatida;

    private TipoPontoEnum tipoBatido;

    @ManyToOne
    private Usuario usuario;

    public Ponto() {
    }

    public int getIdPonto() {
        return idPonto;
    }

    public void setIdPonto(int idPonto) {
        this.idPonto = idPonto;
    }

    public LocalDateTime getHorarioBatida() {
        return horarioBatida;
    }

    public void setHorarioBatida(LocalDateTime horarioBatida) {
        this.horarioBatida = horarioBatida;
    }

    public TipoPontoEnum getTipoBatido() {
        return tipoBatido;
    }

    public void setTipoBatido(TipoPontoEnum tipoBatido) {
        this.tipoBatido = tipoBatido;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
