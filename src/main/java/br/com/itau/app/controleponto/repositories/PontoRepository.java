package br.com.itau.app.controleponto.repositories;


import br.com.itau.app.controleponto.models.Ponto;
import br.com.itau.app.controleponto.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface PontoRepository extends CrudRepository<Ponto, Integer> {
    Iterable<Ponto> findAllByUsuario(Usuario usuario);
}
