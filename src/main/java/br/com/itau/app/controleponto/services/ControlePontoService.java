package br.com.itau.app.controleponto.services;

import br.com.itau.app.controleponto.enums.TipoPontoEnum;
import br.com.itau.app.controleponto.models.Ponto;
import br.com.itau.app.controleponto.models.Usuario;
import br.com.itau.app.controleponto.models.dtos.ControlePontoRespostaDTO;
import br.com.itau.app.controleponto.repositories.PontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDateTime;

@Service
public class ControlePontoService {


    @Autowired
    PontoRepository pontoRepository;
    @Autowired
    UsuarioService usuarioService;

    public Ponto baterPonto(Ponto ponto) {
        Usuario usuario = usuarioService.buscarUsuario(ponto.getUsuario().getId());
        ponto.setUsuario(usuario);
        return pontoRepository.save(ponto);
    }


    public ControlePontoRespostaDTO consultarPonto(int idUsuario) {
        LocalDateTime entrada = null;
        LocalDateTime saida = null;
        Long quantidadeHoras = (long) 0;
        ControlePontoRespostaDTO pontoRespostaDTO = new ControlePontoRespostaDTO();


        Usuario usuario = usuarioService.buscarUsuario(idUsuario);
        Iterable<Ponto> pontosUsuario = pontoRepository.findAllByUsuario(usuario);

        for (Ponto ponto : pontosUsuario) {
            if (ponto.getTipoBatido() == TipoPontoEnum.ENTRADA) {
                entrada = ponto.getHorarioBatida();
            } else {
                saida = ponto.getHorarioBatida();
            }

            if (entrada != null && saida != null) {
                {
                    Duration duracao = Duration.between(entrada, saida);
                    quantidadeHoras += duracao.toMinutes();
                    entrada = null;
                    saida = null;
                }
            }
        }

        pontoRespostaDTO.setPontosUsuario(pontosUsuario);
        pontoRespostaDTO.setTotalHorasTralhadas(new BigDecimal((double) quantidadeHoras / 60)
                .setScale(2, RoundingMode.HALF_UP));
        return pontoRespostaDTO;
    }
}