package br.com.itau.app.controleponto.models.dtos;

import br.com.itau.app.controleponto.models.Ponto;

import java.math.BigDecimal;

public class ControlePontoRespostaDTO {

    Iterable<Ponto> pontosUsuario;
    BigDecimal totalHorasTralhadas;

    public ControlePontoRespostaDTO() {
    }

    public Iterable<Ponto> getPontosUsuario() {
        return pontosUsuario;
    }

    public void setPontosUsuario(Iterable<Ponto> pontosUsuario) {
        this.pontosUsuario = pontosUsuario;
    }

    public BigDecimal getTotalHorasTralhadas() {
        return totalHorasTralhadas;
    }

    public void setTotalHorasTralhadas(BigDecimal totalHorasTralhadas) {
        this.totalHorasTralhadas = totalHorasTralhadas;
    }

}
