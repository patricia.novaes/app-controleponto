package br.com.itau.app.controleponto.controllers;

import br.com.itau.app.controleponto.models.Ponto;
import br.com.itau.app.controleponto.models.dtos.ControlePontoRespostaDTO;
import br.com.itau.app.controleponto.services.ControlePontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/ponto")


public class ControlePontoController {

    @Autowired
    ControlePontoService controlePontoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Ponto baterPonto(@RequestBody @Valid Ponto ponto){
        return controlePontoService.baterPonto(ponto);
    }

    @GetMapping("/{id}")
    public ControlePontoRespostaDTO pesquisarApontamentos(@PathVariable(name = "id") int id){
        return controlePontoService.consultarPonto(id);
    }

}
