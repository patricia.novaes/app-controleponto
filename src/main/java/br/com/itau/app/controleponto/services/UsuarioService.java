package br.com.itau.app.controleponto.services;

import br.com.itau.app.controleponto.models.Usuario;
import br.com.itau.app.controleponto.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

    @Service
    public class UsuarioService {

        @Autowired
        UsuarioRepository usuarioRepository;

        public Usuario criarUsuario(Usuario usuario) {
            return usuarioRepository.save(usuario);
        }

        public Iterable<Usuario> listarUsuarios() {
            return usuarioRepository.findAll();
        }

        public Usuario buscarUsuario(int id) {
            Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);
            if (optionalUsuario.isPresent()) {
                return optionalUsuario.get();
            }
            throw new RuntimeException("Usuario não encontrado");

        }

        public Usuario editarUsuario(int id, Usuario usuarioDTO) {
            Usuario usuario = buscarUsuario(id);
            usuario.setCPF(usuarioDTO.getCPF());
            usuario.setEmail(usuarioDTO.getEmail());
            usuario.setNomeUsuario(usuarioDTO.getNomeUsuario());
            return criarUsuario(usuario);
        }
    }
