package br.com.itau.app.controleponto.models;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "O dado nome do usuário é obrigatório")
    @NotEmpty(message = "O dado nome do usuário é obrigatório")
    private String nomeUsuario;


    @NotNull(message = "O CPF é obrigatório")
    @NotEmpty(message = "O CPF é obrigatório")
    @CPF(message = "O CPF informado é inválido")
    private String CPF;


    @NotNull(message = "O dado email é obrigatório")
    @NotEmpty(message = "O dado email é obrigatório")
    @Email(message = "O email informado é inválido")
    private String email;


    //@NotNull(message = "O dado data de cadastro é obrigatório")
    private LocalDate dataCadastro;

    public Usuario() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
}
