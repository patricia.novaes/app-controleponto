package br.com.itau.app.controleponto.controllers;

import br.com.itau.app.controleponto.models.Usuario;
import br.com.itau.app.controleponto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
    @Autowired
    UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario criarUsuario(@RequestBody @Valid Usuario usuario) {
        return usuarioService.criarUsuario(usuario);
    }

    @GetMapping
    public Iterable<Usuario> listarUsuarios() {
        return usuarioService.listarUsuarios();
    }

    @GetMapping("/{id}")
    public Usuario buscarUsuario(@PathVariable(name = "id") int id) {
        try {
            return usuarioService.buscarUsuario(id);
        } catch (RuntimeException re) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, re.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Usuario editarUsuario(@PathVariable int id, @RequestBody @Valid Usuario usuarioDTO) {
        try {
            return usuarioService.editarUsuario(id, usuarioDTO);
        } catch (RuntimeException re) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, re.getMessage());
        }

    }
}

