package br.com.itau.app.controleponto.repositories;

import br.com.itau.app.controleponto.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

}
