package br.com.itau.app.controleponto.services;

import br.com.itau.app.controleponto.enums.TipoPontoEnum;
import br.com.itau.app.controleponto.models.Ponto;
import br.com.itau.app.controleponto.models.Usuario;
import br.com.itau.app.controleponto.models.dtos.ControlePontoRespostaDTO;
import br.com.itau.app.controleponto.repositories.PontoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

@SpringBootTest
public class ControlePontoServiceTest {

    @MockBean
    private PontoRepository pontoRepository;

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private ControlePontoService pontoService;

    Usuario usuarioTeste;
    Usuario usuarioEntrada;

    Ponto pontoTesteEntrada;
    Ponto pontoTesteSaida;
    Iterable<Ponto> pontos;

    @BeforeEach
    public void Setup() {

        usuarioTeste = new Usuario();
        usuarioEntrada = new Usuario();
        pontoTesteEntrada = new Ponto();
        pontoTesteSaida = new Ponto();

        usuarioTeste.setNomeUsuario("Renata");
        usuarioTeste.setEmail("renata@gmail.com");
        usuarioTeste.setCPF("618.200.390-13");
        usuarioTeste.setDataCadastro(LocalDate.now());
        usuarioTeste.setId(2);


        usuarioEntrada.setId(2);

        pontoTesteEntrada.setIdPonto(1);
        pontoTesteEntrada.setUsuario(usuarioTeste);
        pontoTesteEntrada.setTipoBatido(TipoPontoEnum.ENTRADA);
        pontoTesteEntrada.setHorarioBatida(LocalDateTime.parse("2020-07-05T02:00:00"));

        pontoTesteSaida.setIdPonto(1);
        pontoTesteSaida.setUsuario(usuarioTeste);
        pontoTesteSaida.setTipoBatido(TipoPontoEnum.SAIDA);
        pontoTesteSaida.setHorarioBatida(LocalDateTime.parse("2020-07-05T10:30:00"));

        Mockito.when(usuarioService.buscarUsuario(Mockito.anyInt())).thenReturn(usuarioTeste);
    }

    @Test
    public void testarBaterPonto() {
        Mockito.when(pontoRepository.save(Mockito.any(Ponto.class))).thenReturn(pontoTesteEntrada);

        Ponto pontoObjeto = pontoService.baterPonto(pontoTesteEntrada);

        Assertions.assertEquals(pontoObjeto.getUsuario(), usuarioTeste);
    }

    @Test
    public void testarConsultarPonto(){
        pontos = Arrays.asList(pontoTesteEntrada, pontoTesteSaida);
        Mockito.when(pontoRepository.findAllByUsuario(Mockito.any(Usuario.class))).thenReturn(pontos);

        ControlePontoRespostaDTO controlePontoRespostaDTO = pontoService.consultarPonto(2);
        Assertions.assertEquals(controlePontoRespostaDTO.getTotalHorasTralhadas(), BigDecimal.valueOf(8.50)
                .setScale(2, RoundingMode.HALF_UP));
    }


}
