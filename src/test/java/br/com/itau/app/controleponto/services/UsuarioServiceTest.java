package br.com.itau.app.controleponto.services;

import br.com.itau.app.controleponto.models.Usuario;
import br.com.itau.app.controleponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

    @SpringBootTest
    public class UsuarioServiceTest {

        @MockBean
        private UsuarioRepository usuarioRepository;

        @Autowired
        private UsuarioService usuarioService;

        Usuario usuario;
        Usuario usuarioTeste;

        @BeforeEach
        public void Setup() {
            LocalDate localDate = LocalDate.now();

            usuario = new Usuario();
            usuario.setNomeUsuario("Renata");
            usuario.setEmail("renata@gmail.com");
            usuario.setCPF("233.427.338-45");
            usuario.setDataCadastro(localDate);

            usuarioTeste = new Usuario();
            usuarioTeste.setNomeUsuario("Renata");
            usuarioTeste.setEmail("renata@gmail.com");
            usuarioTeste.setCPF("618.200.390-13");
            usuarioTeste.setDataCadastro(localDate);
            usuarioTeste.setId(2);
        }

        @Test
        public void testarListarUsuario() {
            Iterable<Usuario> usuarios = Arrays.asList(usuario);
            Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);

            Iterable<Usuario> usuarioObjeto = usuarioService.listarUsuarios();

            Assertions.assertEquals(usuarios, usuarioObjeto);
        }

        @Test
        public void testarCriarUsuario() {
            Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuarioTeste);

            Usuario usuarioObjeto = usuarioService.criarUsuario(usuario);
            Assertions.assertEquals(usuarioObjeto.getDataCadastro(), usuarioTeste.getDataCadastro());
        }

        @Test
        public void testarBuscarUsuario() {
            Optional<Usuario> usuarioOptional = Optional.of(usuario);
            Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
            Usuario usuarioObjeto = usuarioService.buscarUsuario(1);
            Assertions.assertEquals(usuario, usuarioObjeto);
        }

        @Test
        public void testarBuscarUsuarioInexistente() {
            Optional<Usuario> usuarioOptional = Optional.empty();
            Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
            Assertions.assertThrows(RuntimeException.class, () -> {usuarioService.buscarUsuario(222); });
        }

        @Test
        public void testarEditarUsuario() {
            Usuario usuarioDTO = new Usuario();
            usuario.setId(2);
            Optional<Usuario> usuarioOptional = Optional.of(usuario);
            Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);

            usuarioTeste.setId(2);
            usuarioTeste.setCPF("963.736.193-62");
            usuarioTeste.setEmail("joao@email.com");
            usuarioTeste.setNomeUsuario("Joao");
            Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuarioTeste);

            usuarioDTO.setCPF("963.736.193-62");
            usuarioDTO.setEmail("joao@email.com");
            usuarioDTO.setNomeUsuario("Joao");
            Usuario usuarioObjeto = usuarioService.editarUsuario(2, usuarioDTO);

            Assertions.assertEquals(2, usuarioObjeto.getId());
        }
    }
