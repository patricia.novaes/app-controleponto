package br.com.itau.app.controleponto.controllers;


import br.com.itau.app.controleponto.enums.TipoPontoEnum;
import br.com.itau.app.controleponto.models.Ponto;
import br.com.itau.app.controleponto.models.Usuario;
import br.com.itau.app.controleponto.models.dtos.ControlePontoRespostaDTO;
import br.com.itau.app.controleponto.services.ControlePontoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

@WebMvcTest(ControlePontoController.class)
public class ControlePontoControllerTest {

    @MockBean
    private ControlePontoService controlePontoService;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuarioObjetoEntrada;
    Usuario usuarioObjetoTeste;
    Ponto pontoEntrada;
    Ponto pontoSaida;

    @BeforeEach
    public void Setup() {
        usuarioObjetoTeste = new Usuario();

        usuarioObjetoEntrada = new Usuario();

        usuarioObjetoEntrada.setId(2);

        usuarioObjetoTeste.setNomeUsuario("Rodrigo");
        usuarioObjetoTeste.setEmail("rodrigo@gmail.com");
        usuarioObjetoTeste.setCPF("41937206084");
        usuarioObjetoTeste.setDataCadastro(LocalDate.now());
        usuarioObjetoTeste.setId(2);

        pontoEntrada = new Ponto();
        pontoEntrada.setIdPonto(1);
        pontoEntrada.setUsuario(usuarioObjetoEntrada);
        pontoEntrada.setTipoBatido(TipoPontoEnum.ENTRADA);
        pontoEntrada.setHorarioBatida(LocalDateTime.parse("2020-07-05T02:00:00"));


        pontoSaida = new Ponto();
        pontoSaida.setIdPonto(1);
        pontoSaida.setUsuario(usuarioObjetoEntrada);
        pontoSaida.setTipoBatido(TipoPontoEnum.SAIDA);
        pontoSaida.setHorarioBatida(LocalDateTime.parse("2020-07-05T10:30:00"));

    }

    @Test
    public void testarBaterPonto() throws Exception {

        Mockito.when(controlePontoService.baterPonto(Mockito.any(Ponto.class))).thenReturn(pontoEntrada);
        pontoEntrada.setHorarioBatida(null);

        ObjectMapper mapper = new ObjectMapper();
        String pontoJson = mapper.writeValueAsString(pontoEntrada);

        pontoJson = pontoJson
                .replace("\"horarioBatida\":null,", "\"horarioBatida\":\"2020-07-05T02:00:00\",");

        pontoEntrada.setHorarioBatida(LocalDateTime.parse("2020-07-05T02:00:00"));

        Mockito.when(controlePontoService.baterPonto(Mockito.any(Ponto.class))).thenReturn(pontoEntrada);

        mockMvc.perform(MockMvcRequestBuilders.post("/ponto")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pontoJson))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarConsultarHorasTrabalhadas() throws Exception {

        Iterable<Ponto> pontos = Arrays.asList(pontoEntrada, pontoSaida);

        ControlePontoRespostaDTO pontoRespostaDTO = new ControlePontoRespostaDTO();

        pontoRespostaDTO.setTotalHorasTralhadas(BigDecimal.valueOf(7.50)
                .setScale(2, RoundingMode.HALF_UP));

        pontoRespostaDTO.setPontosUsuario(pontos);

        Mockito.when(controlePontoService.consultarPonto(Mockito.anyInt())).thenReturn(pontoRespostaDTO);

        mockMvc.perform(MockMvcRequestBuilders.get("/ponto/2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.totalHorasTralhadas", CoreMatchers.equalTo(7.50)));

    }


}
