package br.com.itau.app.controleponto.controllers;

import br.com.itau.app.controleponto.models.Usuario;
import br.com.itau.app.controleponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuario;
    Usuario usuarioTeste;
    LocalDate localDate;


    @BeforeEach
    public void Setup() {

        localDate = LocalDate.now();
        usuario = new Usuario();
        usuario.setNomeUsuario("Rodrigo");
        usuario.setEmail("rodrigo@gmail.com");
        usuario.setCPF("41937206084");
        usuario.setDataCadastro(localDate);

        usuarioTeste = new Usuario();
        usuarioTeste.setNomeUsuario("Maria");
        usuarioTeste.setEmail("maria@gmail.com");
        usuarioTeste.setCPF("75123467009");
        usuarioTeste.setDataCadastro(localDate);
        usuarioTeste.setId(2);
    }


    @Test
    public void testarCriarUsuario() throws Exception {
        usuario.setDataCadastro(null);
        usuario.setId(1);

        ObjectMapper mapper = new ObjectMapper();
        String usuarioJson = mapper.writeValueAsString(usuario);
        usuarioJson = usuarioJson.replace("\"dataCadastro\":null,",
                "\"dataCadastro\":\"2020-07-05\",");

        usuario.setDataCadastro(LocalDate.of(2020, 07, 05));

        Mockito.when(usuarioService.criarUsuario(Mockito.any(Usuario.class))).thenReturn(usuario);
        mockMvc.perform(MockMvcRequestBuilders.post("/usuario")
                .contentType(MediaType.APPLICATION_JSON)
                .content(usuarioJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

}

