O APP Controle de ponto é composto pelos endpoints abaixo: 


*************************** USUARIO *************************** 

Endpoint 1 - Incluir um usuário utilizando o método POST através do endereço: localhost:8080/usuario/
Exemplo dados de entrada:
Body - raw - json:
    {
        "nomeUsuario":"Patricia Novaes",
        "cpf":"896.546.878-79",
        "email":"pati@gmail.com",
        "dataCadastro":"2020-07-05"
    }
    

Endpoint 2 - Consultar lista de usuários utilizando o método GET através do endereço: localhost:8080/usuario/


Endpoint 3 - Consultar as informações de um usuário utilizando o método GET através do endereço: localhost:8080/usuario/{id}


Endpoint 4 - Alterar as informações de um usuário utilizando o método PUT através do endereço: localhost:8080/usuario/{id}
Exemplo dados de entrada:
Body - raw - json:
    {
        "nomeUsuario":"Patricia Novaes",
        "cpf":"896.546.878-79",
        "email":"pati@gmail.com"
    }





***************************  O PONTO ELETRONICO  *************************** 

Endpoint 1 - Incluir o registro de ponto de entrada utilizando o método POST através do endereço: localhost:8080/ponto
Exemplo dados de entrada:
Body - raw - json:
    {
        "horarioBatida":"2020-07-05T19:19:00",
        "tipoBatido": "ENTRADA",
        "usuario":{
             "id":"1"
         }
    }
    
    

Endpoint 2 - Incluir o registro de ponto de saída utiizando o método POST através do endereço: localhost:8080/ponto
Exemplo dados de entrada:
Body - raw - json:
    {
        "horarioBatida":"2020-07-05T22:19:00",
        "tipoBatido": "SAIDA",
        "usuario":{
            "id":"1"
        }
    }

Endpoint 3 - Consultar a lista de registro de ponto de um usuário utilizando o método GET através do endereço: localhost:8080/ponto/{id}



